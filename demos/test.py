#!/usr/bin/env python2
#
# Example to compare the faces in two images.
# Brandon Amos
# 2015/09/29
#
# Copyright 2015-2016 Carnegie Mellon University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time
import re
start = time.time()
from os import listdir
from os.path import isfile, join
from sklearn.cluster import AgglomerativeClustering
import argparse
import cv2
import os

import numpy as np

np.set_printoptions(precision=2)

from sklearn.cluster import DBSCAN

import openface

fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, '..', 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

parser = argparse.ArgumentParser()

parser.add_argument('imgs', type=str, nargs='+', help="Input images.")
parser.add_argument('--dlibFacePredictor', type=str, help="Path to dlib's face predictor.",
                    default=os.path.join(dlibModelDir, "shape_predictor_68_face_landmarks.dat"))
parser.add_argument('--networkModel', type=str, help="Path to Torch network model.",
                    default=os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'))
parser.add_argument('--imgDim', type=int,
                    help="Default image dimension.", default=96)
parser.add_argument('--verbose', action='store_true')

args = parser.parse_args()

if args.verbose:
    print("Argument parsing and loading libraries took {} seconds.".format(
        time.time() - start))

start = time.time()
align = openface.AlignDlib(args.dlibFacePredictor)
net = openface.TorchNeuralNet(args.networkModel, args.imgDim)
if args.verbose:
    print("Loading the dlib and OpenFace models took {} seconds.".format(
        time.time() - start))

def getRep(imgPath):
    rep = []
    if args.verbose:
        print("Processing {}.".format(imgPath))
    bgrImg = cv2.imread(imgPath)
    path, filename = os.path.split(imgPath)
    if bgrImg is None:
        print("Unable to load image: {}".format(imgPath))
        return None
    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

    if args.verbose:
        print("  + Original size: {}".format(rgbImg.shape))

    start = time.time()
    faces = align.getLargestFaceBoundingBox(rgbImg)
    if faces is None:
        print("Unable to find a face: {}".format(imgPath))
        return None
    if args.verbose:
        print("  + Face detection took {} seconds.".format(time.time() - start))

    start = time.time()
    count = 0
    for bb in faces:
        alignedFace = align.align(args.imgDim, rgbImg, bb,
                                  landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        name = filename.split(".")[0]
        name = name.replace("_","")
        f_name = name + "_" + "2" +"_"+ str(count) + ".jpg"
        cv2.imwrite(f_name, alignedFace)
        if alignedFace is None:
            print("Unable to align image: {}".format(imgPath))
        if args.verbose:
            print("  + Face alignment took {} seconds.".format(time.time() - start))

        start = time.time()
        count = count + 1
        rep.append(net.forward(alignedFace))
    if args.verbose:
        print("  + OpenFace forward pass took {} seconds.".format(time.time() - start))
        print("Representation:")
        print(rep)
        print("-----\n")
    return rep



def processTXT(ImgPath):
    onlyfiles = []
    for f in listdir(ImgPath):
        if isfile(join(ImgPath, f)) and re.search('.txt', f) != None:
            onlyfiles.append(join(ImgPath, f))
    for text in onlyfiles:
        h = open(text, "r")
        a = h.readlines()
        path, filename = os.path.split(text)
        name = filename.split(".")[0]
        name = name.replace("_", "")
        f_name = name + "_" + "1" + ".txt"
        g = open(f_name, "w")
        for i in a:
            g.write(i)
        g.close()
        h.close()

def processJPG(ImgPath):
    onlyfiles = []
    for f in listdir(ImgPath):
        if isfile(join(ImgPath, f)) and re.search('.jpg', f) != None:
            onlyfiles.append(join(ImgPath, f))
            for img in onlyfiles:
                getRep(img)

ImgPath = args.imgs[0]

"""
faceVec = []
name = []
for img in onlyfiles:
    rep = getRep(img)
    if rep != None:
        faceVec.extend(rep)
        name.extend(img)
        """



'''
from sklearn.neighbors import kneighbors_graph
connectivity = kneighbors_graph(faceVec, n_neighbors=3, include_self=False)
db =  AgglomerativeClustering(n_clusters=8, connectivity=connectivity,
                               linkage='ward').fit(faceVec)
labels = db.labels_


for k in np.unique(labels):
    class_member_mask = (labels == k)
    count = 0
    for i in range(0, len(name)):
        if class_member_mask[i]:
            img = cv2.imread(name[i])
            count = count + 100
            cv2.imshow("image" + str(count) + "in cluster" + str(k), img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# Number of clusters in labels, ignoring noise if present.
'''


