import os
from collections import defaultdict
from os import listdir
from os.path import isfile, join

import cv2
import numpy as np

from neuralNet import encoder

fileDir = os.path.dirname(os.path.realpath(__file__))
mac1 = fileDir + "/testImage/Room1_preprocess/mac"
mac2 = fileDir + "/testImage/Room2_preprocess/mac"
face1 = fileDir + "/testImage/Room1_preprocess/faces"
face2 = fileDir + "/testImage/Room2_preprocess/faces"
minimumPics = 10 # need at least 10  face to recognize a person (noise filter)
close = 0.4


def FindStationary(MacOfFilename):
    '''
    macs appear in all frame are stationary wifi
    :param MacOfFilename: macs appear in each frame
    :return:
        station: macs of stationary wifi
    '''
    # the macs appear in all frame are stationary wifi
    stations = MacOfFilename[MacOfFilename.keys()[0]]
    for i in MacOfFilename.keys():
        stations = stations.intersection(MacOfFilename[i])
    return stations


def getMac(ImgPath):
    '''
    :param ImgPath: path to mac folder
    :return:
        room: all macs appear in that room
        MacOfFilename: macs appear in each frame
    '''
    room = []
    onlyfiles = []
    MacOfFilename = defaultdict(set)
    for f in listdir(ImgPath):
        if isfile(join(ImgPath, f)):
            onlyfiles.append(join(ImgPath, f))
    for text in onlyfiles:
        h = open(text, "r")
        a = h.readlines()
        rep = []
        for i in a:
            rep.append(i.replace("\r\n", ""))
        room.append(set(rep))
        path, filename = os.path.split(text)
        photo_name = filename.split("_")[0]
        MacOfFilename[photo_name] = MacOfFilename[photo_name].union(set(rep))
        h.close()

    stations = FindStationary(MacOfFilename)
    for station in stations:
        for i in MacOfFilename.keys():
            MacOfFilename[i].remove(station)
    for photo in room:
        for station in stations:
            photo.remove(station)
    return room, MacOfFilename



def pos_customers(mac1, mac2):
    '''
    :param mac1: path to mac directory of room 1
    :param mac2: path to mac directory of room 2
    :return:
        customer_macs: all the mac without stationary
        MacOfFile_room1, MacOfFile_room2: mac in each frame (frame id)
    '''
    room1, MacOfFile_room1 = getMac(mac1)
    allMac1 = set()
    room2, MacOfFile_room2 = getMac(mac2)
    allMac2 = set()
    for i in room1:
        allMac1 = allMac1.union(i)
    for i in room2:
        allMac2 = allMac2.union(i)
    customer_macs = allMac1.intersection(allMac2)
    return customer_macs, MacOfFile_room1, MacOfFile_room2


def invert(d):
    ret = defaultdict(set)
    for key, values in d.items():
        for value in values:
            ret[value].add(key)
    return ret


def customerMacWithFace(mac1, mac2):
    '''
    map customer (define by mac) to the frame they appear in
    :param mac1: directory of macs in room 1
    :param mac2: directory of macs in room 1
    :return:
        MacToFaceNames: mac (name) to frame id
    '''
    MacToFaceNames = defaultdict(set)
    customer_macs, macOfFileRoom1, macOfFileRoom2 = pos_customers(mac1, mac2)
    # get the face that appear in same frame with macs in each room
    facesAppearWithMac1 = invert(macOfFileRoom1)
    facesAppearWithMac2 = invert(macOfFileRoom2)
    for customer in customer_macs:
        # for each mac (name)
        # get the face appear in same frame with that mac
        MacToFaceNames[customer] = MacToFaceNames[customer].union(facesAppearWithMac1[customer])
        MacToFaceNames[customer] = MacToFaceNames[customer].union(facesAppearWithMac2[customer])

    for customer in MacToFaceNames.keys():
        # noise filter
        if len(MacToFaceNames[customer]) < minimumPics:
            MacToFaceNames.pop(customer)
    return MacToFaceNames


def macToFacePath(mac1, mac2, face1, face2):
    rep = defaultdict(set)
    customer_PhotoName = customerMacWithFace(mac1, mac2)
    FacePath1 = [join(face1, f) for f in listdir(face1) if isfile(join(face1, f))]
    FacePath2 = [join(face2, f) for f in listdir(face2) if isfile(join(face2, f))]
    for customerMac in customer_PhotoName.keys():
        for name in customer_PhotoName[customerMac]:
            for path in FacePath1:
                os_path, faceName = os.path.split(path)
                if name == faceName.split("_")[0]:
                    rep[customerMac].add(path)
            for path in FacePath2:
                os_path, faceName = os.path.split(path)
                if name == faceName.split("_")[0]:
                    rep[customerMac].add(path)
    return rep


def pp(matrix):
    for i in matrix.keys():
        l = []
        for j in matrix[i]:
            l.append(matrix[i][j])
        print l


def findCenter(matrix):
    curCenter = None
    curNumberOfSupport = -1
    for i in matrix.keys():
        l = []
        for j in matrix[i]:
            l.append(matrix[i][j])
        tmp = sum(l)
        if tmp > curNumberOfSupport:
            curNumberOfSupport = tmp
            curCenter = i
    return curCenter, curNumberOfSupport


def bestPicInMAC(pics, picToRep):
    D = defaultdict(lambda: defaultdict(lambda: -1))
    count = defaultdict(lambda: defaultdict(lambda: -1))
    for pic1 in pics:
        for pic2 in pics:
            d = picToRep[pic1] - picToRep[pic2]
            D[pic1][pic2] = np.dot(d, d)
            if D[pic1][pic2] < close:
                count[pic1][pic2] = 1
            else:
                count[pic1][pic2] = 0
    center, numberOfSupport = findCenter(count)
    return center
    '''
    img = cv2.imread(center)
    cv2.imshow("center",img)
    for support in count[center]:
        if count[center][support] == 1:
            img = cv2.imread(support)
            cv2.imshow(support, img)
            cv2.waitKey(0)
    cv2.waitKey(0)
    '''


def showAllFaceInMac(mac, pics):
    for pic in pics:
        img = cv2.imread(pic)
        cv2.imshow(pic, img)
        cv2.waitKey(200)
    cv2.waitKey(0)


# the number of photo of a person recognised < number of time their MAC appear
def MacToFace(mac1, mac2, face1, face2):
    # cluster mac (name) with picture (path)
    macToPic = macToFacePath(mac1, mac2, face1, face2)
    # pic (path) with vector (ndarray)
    picToRep = defaultdict(set)
    E = encoder()
    for mac in macToPic.keys(): # pac is mac address (or name)
        for pic in macToPic[mac]: # pic is path
            picToRep[pic] = E.getRep(pic, preprpcessed=True)
    centers = defaultdict()
    for mac in macToPic.keys():
        centers[mac] = bestPicInMAC(macToPic[mac], picToRep)
    return centers


#macToPic = macToFacePath(mac1, mac2, face1, face2)
#showAllFaceInMac('vinh', macToPic['vinh'])

centers = MacToFace(mac1, mac2, face1, face2)
for mac in centers:
    print mac
    img = cv2.imread(centers[mac])
    cv2.imshow(mac, img)
    cv2.waitKey(0)

"""
picToRep = defaultdict(set)
E = encoder()
for pic in macToPic[mac]:
    picToRep[pic] = E.getRep(pic, preprpcessed=True)
for (pic1, pic2) in itertools.combinations(macToPic[mac], 2):
    d = picToRep[pic1] - picToRep[pic2]
    a = cv2.imread(pic1)
    b = cv2.imread(pic2)
    cv2.imshow("image1", a)
    cv2.imshow("image2", b)
    print(
        "  + Squared l2 distance between representations: {:0.3f}".format(np.dot(d, d)))
    cv2.waitKey(0)

"""
